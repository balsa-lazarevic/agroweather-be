# Agroweather - API

Agroweather API is an app written in Node JS and MySQL that represents backend part of the Agroweather app.

## Prepare environment


### Import and configure database
Import database ```database.sql``` using mysqlimport command, your existing database and path to database.sql file.

```bash 
mysqlimport yourdatabasename database.sql
```

Configure ```config/environments/development/database.json``` file and enter proper connection details to your mysql server.

Configure ```config/environments/development/server.json``` to serve the app from ```127.0.0.1``` (localhost) or your custom IP address.

## Installation

Make sure you're running Node >= 12.

Use the npm package manager or yarn to install packages.

```bash
yarn install
```

## Usage

```bash
# Run build
yarn build

# Run development
yarn develop

# Run production
yarn start
```

## Hints
### Running in production
To run the app in production use package ```pm2``` for linux. It runs the app in background and there's no need to have the terminal open.

Install
```bash
npm install pm2 -g
```
Run (from inside the project)
```bash 
pm2 start "NODE_ENV=production yarn start" --name Backend
```

## Contributing
Pull requests are welcome. For major changes, please open an issue first to discuss what you would like to change.

## License
[MIT](https://choosealicense.com/licenses/mit/)