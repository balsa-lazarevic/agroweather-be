import requests
import json
import time
from datetime import datetime

cmsBaseURL = 'http://localhost:1337'
hereAPIKey = 'c3E22NZrlAc_M3eYFw4E3Nj6SoJ65VNp_VXqJ0aSgpQ'
openWeatherAPIKey = '50b07a6403eb00464478a09f0c78dfe6'
darkSkyAPIKey = '245e6d8db0ed2026709ace45c6b6b9a4'

def submitMeasurement(params):
    #print(params)
    submitURL = cmsBaseURL + '/measurements'
    submitHeaders = {"Authorization": "Bearer " + jwt}
    submitResponse = requests.post(submitURL, data = params, headers = submitHeaders)
    #print(json.loads(submitResponse.text))

def submitForecast(params):
    #print(json.dumps(params, indent=2))
    submitURL = cmsBaseURL + '/forecasts'
    submitHeaders = {"Authorization": "Bearer " + jwt, 'Content-type': 'application/json', 'Accept': 'application/json'}
    submitResponse = requests.post(submitURL, data = json.dumps(params), headers = submitHeaders)
    # print(json.loads(submitResponse.text))

def getCurrentHereWeather(params):
    response = requests.get('https://weather.ls.hereapi.com/weather/1.0/report.json?product=observation&latitude=' + str(params['lat']) + '&longitude=' + str(params['lon']) + '&oneobservation=true&apiKey=' + hereAPIKey)
    responseJSON = json.loads(response.content)

    currentWeather = responseJSON['observations']['location'][0]['observation'][0]

    #Format
    precipitation = "0" if currentWeather["precipitation1H"] == "*" else currentWeather["precipitation1H"]
    precipitationRain = 0
    precipitationSnow = 0
    precipitationType = "rain"
    if "snowFall" in currentWeather:
        if currentWeather["rainFall"] != "*":
            precipitationRain = precipitation
        if currentWeather["snowFall"] != "*":
            precipitationSnow = precipitation

    postData = {
        "location": int(params['id']),
        "Time": currentWeather["utcTime"],
        "Source": "here",
        "Temperature": currentWeather["temperature"],
        "Humidity": currentWeather["humidity"],
        "Pressure": "0" if currentWeather["barometerPressure"] == "*" else currentWeather["barometerPressure"],
        "WindDirection": currentWeather["windDirection"],
        "WindSpeed": currentWeather["windSpeed"],
        "RainRate": precipitationRain,
        "SnowRate": precipitationSnow,
        "Icon": currentWeather["iconName"],
    }
    submitMeasurement(postData)
    return postData

def getCurrentOpenWeather(params):
    response = requests.get('https://api.openweathermap.org/data/2.5/weather?lat=' + str(params['lat']) + '&lon=' + str(params['lon']) + '&units=metric&appid=' + openWeatherAPIKey)
    responseJSON = json.loads(response.content)

    currentWeather = responseJSON

    #Format
    windDeg = 0
    windSpeed = 0
    if "wind" in currentWeather:
        if "deg" in currentWeather["wind"]:
            windDeg = currentWeather["wind"]["deg"]
        if "speed" in currentWeather["wind"]:
            windSpeed = currentWeather["wind"]["speed"]

    rainRate = 0
    if "rain" in currentWeather:
        if "1h" in currentWeather["rain"]:
            rainRate = currentWeather["rain"]["1h"]
        if "3h" in currentWeather["rain"]:
            rainRate = currentWeather["rain"]["3h"]

    snowRate = 0
    if "snow" in currentWeather:
        if "1h" in currentWeather["snow"]:
            snowRate = currentWeather["snow"]["1h"]
        if "3h" in currentWeather["snow"]:
            snowRate = currentWeather["snow"]["3h"]
        

    postData = {
        "location": int(params['id']),
        "Time": str(datetime.utcfromtimestamp(int(currentWeather["dt"]))),
        "Source": "openweathermap",
        "Temperature": currentWeather["main"]["temp"],
        "Humidity": currentWeather["main"]["humidity"],
        "Pressure": currentWeather["main"]["pressure"],
        "WindDirection": windDeg,
        "WindSpeed": windSpeed,
        "RainRate": rainRate,
        "SnowRate": snowRate,
        "Icon": currentWeather["weather"][0]["icon"],
    }
    
    submitMeasurement(postData)
    return postData

def getCurrentDarkSky(params):
    response = requests.get('https://api.darksky.net/forecast/' + darkSkyAPIKey + '/' + str(params['lat']) + ',' + str(params['lon']) + '?units=ca')
    responseJSON = json.loads(response.content)

    currentWeather = responseJSON['currently']
    #Format 
    precip = True if "precipType" in currentWeather else False
    rainRate = False
    snowRate = False
    if precip:
        if currentWeather["precipType"] == 'rain':
            rainRate = True
        if currentWeather["precipType"] == 'snow':
            snowRate = True

    postData = {
        "location": int(params['id']),
        "Time": str(datetime.utcfromtimestamp(int(currentWeather["time"]))),
        "Source": "darksky",
        "Temperature": currentWeather["temperature"],
        "Humidity": currentWeather["humidity"],
        "Pressure": currentWeather["pressure"],
        "WindDirection": currentWeather["windBearing"],
        "WindSpeed": currentWeather["windSpeed"],
        "RainRate": currentWeather["precipIntensity"] if rainRate else "0",
        "SnowRate": currentWeather["precipIntensity"] if snowRate else "0",
        "Icon": currentWeather["icon"],
    }
    
    submitMeasurement(postData)
    return postData

def getForecastOpenWeather(params):
    response = requests.get('https://api.openweathermap.org/data/2.5/forecast/daily?lat=' + str(params['lat']) + '&lon=' + str(params['lon']) + '&cnt=7&units=metric&appid=' + openWeatherAPIKey)
    responseJSON = json.loads(response.content)

    forecastWeather = responseJSON["list"]

    postData = {
        "location": int(params['id']),
        "Time": str(datetime.utcfromtimestamp(time.time())),
        "Source": "openweathermap",
        "MeteoData": []
    }

    for forecast in forecastWeather:
        curForecast = {
            "Time": forecast["dt"] * 1000,
            "Temperature": forecast["temp"]["day"],
            "TemperatureMin": forecast["temp"]["min"],
            "TemperatureMax": forecast["temp"]["max"],
            "Humidity": forecast["humidity"],
            "Pressure": forecast["pressure"],
            "WindDirection": forecast["deg"],
            "WindSpeed": forecast["speed"],
            "RainRate": forecast["rain"] if "rain" in forecast else "0",
            "SnowRate": forecast["snow"] if "snow" in forecast else "0",
            "Icon": forecast["weather"][0]["icon"]
        }
        postData["MeteoData"].append(curForecast)
    
    submitForecast(postData)

def getForecastHereWeather(params):
    response = requests.get('https://weather.ls.hereapi.com/weather/1.0/report.json?product=forecast_7days_simple&latitude=' + str(params['lat']) + '&longitude=' + str(params['lon']) + '&oneobservation=true&apiKey=' + hereAPIKey)
    responseJSON = json.loads(response.content)

    forecastWeather = responseJSON['dailyForecasts']['forecastLocation']['forecast']

    postData = {
        "location": int(params['id']),
        "Time": str(datetime.utcfromtimestamp(time.time())),
        "Source": "here",
        "MeteoData": []
    }

    for forecast in forecastWeather:
        #Format
        rainFall = 0
        snowFall = 0
        if "rainFall" in forecast:
            if forecast["rainFall"] != "*":
                rainFall = forecast["rainFall"]
        if "snowFall" in forecast:
            if forecast["snowFall"] != "*":
                snowFall = forecast["snowFall"]

        curForecast = {
            "Time": forecast["utcTime"],
            "Temperature": forecast["highTemperature"],
            "TemperatureMin": forecast["lowTemperature"],
            "TemperatureMax": forecast["highTemperature"],
            "Humidity": forecast["humidity"],
            "Pressure": "0" if forecast["barometerPressure"] == "*" else forecast["barometerPressure"],
            "WindDirection": forecast["windDirection"],
            "WindSpeed": forecast["windSpeed"],
            "RainRate": rainFall,
            "SnowRate": snowFall,
            "Icon": forecast["iconName"]
        }
        postData["MeteoData"].append(curForecast)
    
    submitForecast(postData)

def getForecastDarkSky(params):
    response = requests.get('https://api.darksky.net/forecast/' + darkSkyAPIKey + '/' + str(params['lat']) + ',' + str(params['lon']) + '?units=ca')
    responseJSON = json.loads(response.content)

    forecastWeather = responseJSON['daily']['data']

    postData = {
        "location": int(params['id']),
        "Time": str(datetime.utcfromtimestamp(time.time())),
        "Source": "darksky",
        "MeteoData": []
    }

    for forecast in forecastWeather:
        #Format 
        precip = True if "precipType" in forecast else False
        rainRate = False
        snowRate = False
        if precip:
            if forecast["precipType"] == 'rain':
                rainRate = True
            if forecast["precipType"] == 'snow':
                snowRate = True
                
        curForecast = {
            "Time": forecast["time"],
            "Temperature": forecast["temperatureMax"],
            "TemperatureMin": forecast["temperatureMin"],
            "TemperatureMax": forecast["temperatureMax"],
            "Humidity": forecast["humidity"],
            "Pressure": forecast["pressure"],
            "WindDirection": forecast["windBearing"],
            "WindSpeed": forecast["windSpeed"],
            "RainRate": rainRate,
            "SnowRate": snowRate,
            "Icon": forecast["icon"]
        }
        postData["MeteoData"].append(curForecast)
    
    submitForecast(postData)
 
# Loguje se u strapi - dobija token
loginURL = cmsBaseURL + '/auth/local'
loginData = {'identifier': 'updater', 'password': 'updater'}
loginResponse = requests.post(loginURL, data = loginData)
jwt = json.loads(loginResponse.text)['jwt']

# Pozive sve lokacije za mjerenje
locationsURL = cmsBaseURL + '/services/getLocations'
locationsHeaders = {"Authorization": "Bearer " + jwt}
locationsResponse = requests.get(locationsURL, headers = locationsHeaders)
locations = json.loads(locationsResponse.text)

# Prelazi lokaciju po lokaciju
for location in locations:
    print('--------')
    # Prelazi izvor po izvor - (hereweather, openweather i darksky)
    currentHereWeather = getCurrentHereWeather({'id': location['id'], 'lat': location['Latitude'], 'lon': location['Longitude']})
    currentOpenWeather = getCurrentOpenWeather({'id': location['id'], 'lat': location['Latitude'], 'lon': location['Longitude']})
    currentDarkSky = getCurrentDarkSky({'id': location['id'], 'lat': location['Latitude'], 'lon': location['Longitude']})
    
    forecastHereWeather = getForecastHereWeather({'id': location['id'], 'lat': location['Latitude'], 'lon': location['Longitude']})
    forecastOpenWeather = getForecastOpenWeather({'id': location['id'], 'lat': location['Latitude'], 'lon': location['Longitude']})
    forecastDarkSky = getForecastDarkSky({'id': location['id'], 'lat': location['Latitude'], 'lon': location['Longitude']})
    
    time.sleep(10)