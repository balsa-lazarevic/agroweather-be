import requests
import json
import time
from datetime import datetime

cmsBaseURL = 'http://localhost:1337'
hereAPIKey = 'c3E22NZrlAc_M3eYFw4E3Nj6SoJ65VNp_VXqJ0aSgpQ'
openWeatherAPIKey = '50b07a6403eb00464478a09f0c78dfe6'
darkSkyAPIKey = '245e6d8db0ed2026709ace45c6b6b9a4'

def calculateAccuracy(params):
    accuracy = { "Accuracy1": 0, "Accuracy2": 0, "Accuracy3": 0, "Accuracy4": 0, "Accuracy5": 0, "Accuracy6": 0, "Accuracy7": 0}
    curTemperature = float(params['current']['Temperature'])
    historyData = params['history']
    for i in range(len(historyData)):
        try:
            day = historyData[i]["MeteoData"][i]
            dayTemperature = float(day["Temperature"])
            curAccuracy = 0
            if(curTemperature >= dayTemperature):
                curAccuracy = dayTemperature / curTemperature
            else:
                curAccuracy = curTemperature / dayTemperature
            #print('-------- ' + str(i))
            # print('curTemperature - ' + str(curTemperature) + ' | dayTemperature - ' + str(dayTemperature))
            # print('accuracy - ' + str(curAccuracy))
            accuracy["Accuracy" + str(i + 1)] = round(curAccuracy, 2)
        except IndexError:
            accuracy["Accuracy" + str(i + 1)] = 0
    # print(json.dumps(accuracy, indent=2))
    return accuracy

def getForecastHistory(params):
    # Poziva Forecasts
    forecastsURL = cmsBaseURL + '/forecasts?location=' + str(params['id']) + '&Source=' + params['source'] + '&_limit=7&_sort=Time:DESC'
    forecastsHeaders = {"Authorization": "Bearer " + jwt}
    forecastsResponse = requests.get(forecastsURL, headers = forecastsHeaders)
    forecasts = json.loads(forecastsResponse.text)
    return forecasts

def submitForecast(params):
    #print(json.dumps(params, indent=2))
    submitURL = cmsBaseURL + '/forecasts'
    submitHeaders = {"Authorization": "Bearer " + jwt, 'Content-type': 'application/json', 'Accept': 'application/json'}
    submitResponse = requests.post(submitURL, data = json.dumps(params), headers = submitHeaders)
    # print(json.loads(submitResponse.text))

def getCurrentHereWeather(params):
    response = requests.get('https://weather.ls.hereapi.com/weather/1.0/report.json?product=observation&latitude=' + str(params['lat']) + '&longitude=' + str(params['lon']) + '&oneobservation=true&apiKey=' + hereAPIKey)
    responseJSON = json.loads(response.content)

    currentWeather = responseJSON['observations']['location'][0]['observation'][0]

    #Format
    precipitation = "0" if currentWeather["precipitation1H"] == "*" else currentWeather["precipitation1H"]
    precipitationRain = 0
    precipitationSnow = 0
    precipitationType = "rain"
    if "snowFall" in currentWeather:
        if currentWeather["rainFall"] != "*":
            precipitationRain = precipitation
        if currentWeather["snowFall"] != "*":
            precipitationSnow = precipitation

    postData = {
        "location": int(params['id']),
        "Time": currentWeather["utcTime"],
        "Source": "here",
        "Temperature": currentWeather["temperature"],
        "Humidity": currentWeather["humidity"],
        "Pressure": "0" if currentWeather["barometerPressure"] == "*" else currentWeather["barometerPressure"],
        "WindDirection": currentWeather["windDirection"],
        "WindSpeed": currentWeather["windSpeed"],
        "RainRate": precipitationRain,
        "SnowRate": precipitationSnow,
        "Icon": currentWeather["iconName"],
    }
    return postData

def getCurrentOpenWeather(params):
    response = requests.get('https://api.openweathermap.org/data/2.5/weather?lat=' + str(params['lat']) + '&lon=' + str(params['lon']) + '&units=metric&appid=' + openWeatherAPIKey)
    responseJSON = json.loads(response.content)

    currentWeather = responseJSON

    #Format
    windDeg = 0
    windSpeed = 0
    if "wind" in currentWeather:
        if "deg" in currentWeather["wind"]:
            windDeg = currentWeather["wind"]["deg"]
        if "speed" in currentWeather["wind"]:
            windSpeed = currentWeather["wind"]["speed"]

    snowRate = 0
    if "snow" in currentWeather:
        if "1h" in currentWeather["snow"]:
            snowRate = currentWeather["snow"]["1h"]
        if "3h" in currentWeather["snow"]:
            snowRate = currentWeather["snow"]["3h"]
    
    rainRate = 0
    if "rain" in currentWeather:
        if "1h" in currentWeather["rain"]:
            rainRate = currentWeather["rain"]["1h"]
        if "3h" in currentWeather["rain"]:
            rainRate = currentWeather["rain"]["3h"]
        

    postData = {
        "location": int(params['id']),
        "Time": str(datetime.utcfromtimestamp(int(currentWeather["dt"]))),
        "Source": "openweathermap",
        "Temperature": currentWeather["main"]["temp"],
        "Humidity": currentWeather["main"]["humidity"],
        "Pressure": currentWeather["main"]["pressure"],
        "WindDirection": windDeg,
        "WindSpeed": windSpeed,
        "RainRate": rainRate,
        "SnowRate": snowRate,
        "Icon": currentWeather["weather"][0]["icon"],
    }
    
    return postData

def getCurrentDarkSky(params):
    response = requests.get('https://api.darksky.net/forecast/' + darkSkyAPIKey + '/' + str(params['lat']) + ',' + str(params['lon']) + '?units=ca')
    responseJSON = json.loads(response.content)

    currentWeather = responseJSON['currently']
    #Format 
    precip = True if "precipType" in currentWeather else False
    rainRate = False
    snowRate = False
    if precip:
        if currentWeather["precipType"] == 'rain':
            rainRate = True
        if currentWeather["precipType"] == 'snow':
            snowRate = True

    postData = {
        "location": int(params['id']),
        "Time": str(datetime.utcfromtimestamp(int(currentWeather["time"]))),
        "Source": "darksky",
        "Temperature": currentWeather["temperature"],
        "Humidity": currentWeather["humidity"],
        "Pressure": currentWeather["pressure"],
        "WindDirection": currentWeather["windBearing"],
        "WindSpeed": currentWeather["windSpeed"],
        "RainRate": currentWeather["precipIntensity"] if rainRate else "0",
        "SnowRate": currentWeather["precipIntensity"] if snowRate else "0",
        "Icon": currentWeather["icon"],
    }
    
    return postData

def getForecastOpenWeather(params):
    response = requests.get('https://api.openweathermap.org/data/2.5/forecast/daily?lat=' + str(params['lat']) + '&lon=' + str(params['lon']) + '&cnt=7&units=metric&appid=' + openWeatherAPIKey)
    responseJSON = json.loads(response.content)

    forecastWeather = responseJSON["list"]

    postData = {
        "location": int(params['id']),
        "Time": str(datetime.utcfromtimestamp(time.time())),
        "Source": "openweathermap",
        "MeteoData": [],
        "Accuracy": []
    }

    # Analizira preciznost
    weatherCurrent = getCurrentOpenWeather(params)
    weatherHistory = getForecastHistory({'id': params['id'], 'source': 'openweathermap'})

    accuracy = calculateAccuracy({'current': weatherCurrent, 'history': weatherHistory})    
    postData["Accuracy"] = accuracy
    
    # Preskoci do sjutrasnje prognoze
    skip = 1
    cur = -1 
    for forecast in forecastWeather:
        # Preskace
        cur += 1
        if cur < skip:
            continue

        curForecast = {
            "Time": forecast["dt"] * 1000,
            "Temperature": forecast["temp"]["day"],
            "TemperatureMin": forecast["temp"]["min"],
            "TemperatureMax": forecast["temp"]["max"],
            "Humidity": forecast["humidity"],
            "Pressure": forecast["pressure"],
            "WindDirection": forecast["deg"],
            "WindSpeed": forecast["speed"],
            "RainRate": forecast["rain"] if "rain" in forecast else "0",
            "SnowRate": forecast["snow"] if "snow" in forecast else "0",
            "Icon": forecast["weather"][0]["icon"]
        }
        postData["MeteoData"].append(curForecast)

    # print(json.dumps(postData, indent=2))
    submitForecast(postData)

def getForecastHereWeather(params):
    response = requests.get('https://weather.ls.hereapi.com/weather/1.0/report.json?product=forecast_7days_simple&latitude=' + str(params['lat']) + '&longitude=' + str(params['lon']) + '&oneobservation=true&apiKey=' + hereAPIKey)
    responseJSON = json.loads(response.content)

    forecastWeather = responseJSON['dailyForecasts']['forecastLocation']['forecast']

    postData = {
        "location": int(params['id']),
        "Time": str(datetime.utcfromtimestamp(time.time())),
        "Source": "here",
        "MeteoData": [],
        "Accuracy": [],
    }

    # Analizira preciznost
    weatherCurrent = getCurrentHereWeather(params)
    weatherHistory = getForecastHistory({'id': params['id'], 'source': 'here'})

    accuracy = calculateAccuracy({'current': weatherCurrent, 'history': weatherHistory})    
    postData["Accuracy"] = accuracy

    # Preskoci do sjutrasnje prognoze
    skip = 1 # Ne preskace
    cur = -1 
    for forecast in forecastWeather:
        # Preskace
        cur += 1
        if cur < skip:
            continue

        #Format
        rainFall = 0
        snowFall = 0
        if "rainFall" in forecast:
            if forecast["rainFall"] != "*":
                rainFall = forecast["rainFall"]
        if "snowFall" in forecast:
            if forecast["snowFall"] != "*":
                snowFall = forecast["snowFall"]

        curForecast = {
            "Time": forecast["utcTime"],
            "Temperature": forecast["highTemperature"],
            "TemperatureMin": forecast["lowTemperature"],
            "TemperatureMax": forecast["highTemperature"],
            "Humidity": forecast["humidity"],
            "Pressure": "0" if forecast["barometerPressure"] == "*" else forecast["barometerPressure"],
            "WindDirection": forecast["windDirection"],
            "WindSpeed": forecast["windSpeed"],
            "RainRate": rainFall,
            "SnowRate": snowFall,
            "Icon": forecast["iconName"]
        }
        postData["MeteoData"].append(curForecast)
    
    submitForecast(postData)

def getForecastDarkSky(params):
    response = requests.get('https://api.darksky.net/forecast/' + darkSkyAPIKey + '/' + str(params['lat']) + ',' + str(params['lon']) + '?units=ca')
    responseJSON = json.loads(response.content)

    forecastWeather = responseJSON['daily']['data']

    postData = {
        "location": int(params['id']),
        "Time": str(datetime.utcfromtimestamp(time.time())),
        "Source": "darksky",
        "MeteoData": [],
        "Accuracy": []
    }

    # Analizira preciznost
    weatherCurrent = getCurrentDarkSky(params)
    weatherHistory = getForecastHistory({'id': params['id'], 'source': 'darksky'})

    accuracy = calculateAccuracy({'current': weatherCurrent, 'history': weatherHistory})    
    postData["Accuracy"] = accuracy

    # Preskoci do sjutrasnje prognoze
    skip = 1
    cur = -1 
    for forecast in forecastWeather:
        # Preskace
        cur += 1
        if cur < skip:
            continue

        #Format 
        precip = True if "precipType" in forecast else False
        rainRate = False
        snowRate = False
        if precip:
            if forecast["precipType"] == 'rain':
                rainRate = True
            if forecast["precipType"] == 'snow':
                snowRate = True
                
        curForecast = {
            "Time": int(forecast["time"]) * 1000,
            "Temperature": forecast["temperatureMax"],
            "TemperatureMin": forecast["temperatureMin"],
            "TemperatureMax": forecast["temperatureMax"],
            "Humidity": forecast["humidity"],
            "Pressure": forecast["pressure"],
            "WindDirection": forecast["windBearing"],
            "WindSpeed": forecast["windSpeed"],
            "RainRate": rainRate,
            "SnowRate": snowRate,
            "Icon": forecast["icon"]
        }
        postData["MeteoData"].append(curForecast)
    
    submitForecast(postData)
 
# Loguje se u strapi - dobija token
loginURL = cmsBaseURL + '/auth/local'
loginData = {'identifier': 'updater', 'password': 'updater'}
loginResponse = requests.post(loginURL, data = loginData)
jwt = json.loads(loginResponse.text)['jwt']

# Pozive sve lokacije za mjerenje
locationsURL = cmsBaseURL + '/services/getLocations'
locationsHeaders = {"Authorization": "Bearer " + jwt}
locationsResponse = requests.get(locationsURL, headers = locationsHeaders)
locations = json.loads(locationsResponse.text)

# Za logovanje
curDate = datetime.now()
print()
print(curDate)
# Prelazi lokaciju po lokaciju
for location in locations:
    print('-', location['Title'])
    forecastHereWeather = getForecastHereWeather({'id': location['id'], 'lat': location['Latitude'], 'lon': location['Longitude']})
    forecastOpenWeather = getForecastOpenWeather({'id': location['id'], 'lat': location['Latitude'], 'lon': location['Longitude']})
    forecastDarkSky = getForecastDarkSky({'id': location['id'], 'lat': location['Latitude'], 'lon': location['Longitude']})
    time.sleep(1)