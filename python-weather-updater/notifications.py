import requests
import json
import time
from datetime import datetime

cmsBaseURL = 'http://localhost:1337'
hereAPIKey = 'c3E22NZrlAc_M3eYFw4E3Nj6SoJ65VNp_VXqJ0aSgpQ'
openWeatherAPIKey = '50b07a6403eb00464478a09f0c78dfe6'
darkSkyAPIKey = '245e6d8db0ed2026709ace45c6b6b9a4'

allCurrent = {}

# Loguje se u strapi - dobija token
loginURL = cmsBaseURL + '/auth/local'
loginData = {'identifier': 'updater', 'password': 'updater'}
loginResponse = requests.post(loginURL, data = loginData)
jwt = json.loads(loginResponse.text)['jwt']
authorizationHeaders = {"Authorization": "Bearer " + jwt}

# Pozive sve lokacije za mjerenje
locations = False
locationsURL = cmsBaseURL + '/services/getLocations'
locationsResponse = requests.get(locationsURL, headers = authorizationHeaders)

# Pozive sve notifikacije za provjeru i slanje
notifications = False
notificationsURL = cmsBaseURL + '/notifications?Active=true'
notificationsResponse = requests.get(notificationsURL, headers = authorizationHeaders)

def submitMeasurement(params):
    #print(params)
    submitURL = cmsBaseURL + '/measurements'
    submitHeaders = {"Authorization": "Bearer " + jwt}
    submitResponse = requests.post(submitURL, data = params, headers = authorizationHeaders)
    #print(json.loads(submitResponse.text))
    return True

def sendNotification(params):
    notification = params["notification"]
    print('salje notifikaciju')
    # Poziva callback
    if notification["Method"] == 'callback':
        print("Poziva callback")
        sendCallback = requests.get(notification["MethodRecepient"])

def updateNotification(params):
    print('azurira notifikaciju')
    notification = params["notification"]
    submitURL = cmsBaseURL + '/notifications/' + str(notification["id"])
    preparedData = { 
        "alreadyMet": "true" if params["alreadyMet"] == True else "false"
    }
    submitResponse = requests.put(submitURL, data = preparedData, headers = authorizationHeaders)
    print(json.loads(submitResponse.text))

def getCurrentHereWeather(params):
    try:
        response = requests.get('https://weather.ls.hereapi.com/weather/1.0/report.json?product=observation&latitude=' + str(params['lat']) + '&longitude=' + str(params['lon']) + '&oneobservation=true&apiKey=' + hereAPIKey)
        responseJSON = json.loads(response.content)

        currentWeather = responseJSON['observations']['location'][0]['observation'][0]

        #Format
        precipitation = "0" if currentWeather["precipitation1H"] == "*" else currentWeather["precipitation1H"]
        precipitationRain = 0
        precipitationSnow = 0
        precipitationType = "rain"
        if "snowFall" in currentWeather:
            if currentWeather["rainFall"] != "*":
                precipitationRain = precipitation
            if currentWeather["snowFall"] != "*":
                precipitationSnow = precipitation

        postData = {
            "location": int(params['id']),
            "Time": currentWeather["utcTime"],
            "Source": "here",
            "Temperature": currentWeather["temperature"],
            "Humidity": currentWeather["humidity"],
            "Pressure": "0" if currentWeather["barometerPressure"] == "*" else currentWeather["barometerPressure"],
            "WindDirection": currentWeather["windDirection"],
            "WindSpeed": currentWeather["windSpeed"],
            "RainRate": precipitationRain,
            "SnowRate": precipitationSnow,
            "Icon": currentWeather["iconName"],
        }
        submitMeasurement(postData)

        # Sacuvaj u memoriji za dalje koriscenje
        allCurrent[int(params['id'])]["here"] = postData

        return postData
    except:
        print('Greska prilikom unosa lokacije')
        print('ID', int(params['id']))
        print('Source', 'here')

def getCurrentOpenWeather(params):
    try:
        response = requests.get('https://api.openweathermap.org/data/2.5/weather?lat=' + str(params['lat']) + '&lon=' + str(params['lon']) + '&units=metric&appid=' + openWeatherAPIKey)
        responseJSON = json.loads(response.content)

        currentWeather = responseJSON

        #Format
        windDeg = 0
        windSpeed = 0
        if "wind" in currentWeather:
            if "deg" in currentWeather["wind"]:
                windDeg = currentWeather["wind"]["deg"]
            if "speed" in currentWeather["wind"]:
                windSpeed = currentWeather["wind"]["speed"]

        rainRate = 0
        if "rain" in currentWeather:
            if "1h" in currentWeather["rain"]:
                rainRate = currentWeather["rain"]["1h"]
            if "3h" in currentWeather["rain"]:
                rainRate = currentWeather["rain"]["3h"]

        snowRate = 0
        if "snow" in currentWeather:
            if "1h" in currentWeather["snow"]:
                snowRate = currentWeather["snow"]["1h"]
            if "3h" in currentWeather["snow"]:
                snowRate = currentWeather["snow"]["3h"]
            

        postData = {
            "location": int(params['id']),
            "Time": str(datetime.utcfromtimestamp(int(currentWeather["dt"]))),
            "Source": "openweathermap",
            "Temperature": currentWeather["main"]["temp"],
            "Humidity": currentWeather["main"]["humidity"],
            "Pressure": currentWeather["main"]["pressure"],
            "WindDirection": windDeg,
            "WindSpeed": windSpeed,
            "RainRate": rainRate,
            "SnowRate": snowRate,
            "Icon": currentWeather["weather"][0]["icon"],
        }
        
        submitMeasurement(postData)

        # Sacuvaj u memoriji za dalje koriscenje
        allCurrent[int(params['id'])]["openweathermap"] = postData

        return postData
    except:
        print('Greska prilikom unosa lokacije')
        print('ID', int(params['id']))
        print('Source', 'openweathermap')

def getCurrentDarkSky(params):
    try:
        response = requests.get('https://api.darksky.net/forecast/' + darkSkyAPIKey + '/' + str(params['lat']) + ',' + str(params['lon']) + '?units=ca')
        responseJSON = json.loads(response.content)

        currentWeather = responseJSON['currently']
        #Format 
        precip = True if "precipType" in currentWeather else False
        rainRate = False
        snowRate = False
        if precip:
            if currentWeather["precipType"] == 'rain':
                rainRate = True
            if currentWeather["precipType"] == 'snow':
                snowRate = True

        postData = {
            "location": int(params['id']),
            "Time": str(datetime.utcfromtimestamp(int(currentWeather["time"]))),
            "Source": "darksky",
            "Temperature": currentWeather["temperature"],
            "Humidity": currentWeather["humidity"],
            "Pressure": currentWeather["pressure"],
            "WindDirection": currentWeather["windBearing"],
            "WindSpeed": currentWeather["windSpeed"],
            "RainRate": currentWeather["precipIntensity"] if rainRate else "0",
            "SnowRate": currentWeather["precipIntensity"] if snowRate else "0",
            "Icon": currentWeather["icon"],
        }
        # Sacuvaj u bazi
        submitMeasurement(postData)

        # Sacuvaj u memoriji za dalje koriscenje
        allCurrent[int(params['id'])]["darksky"] = postData

        return postData
    except:
        print('Greska prilikom unosa lokacije')
        print('ID', int(params['id']))
        print('Source', 'darksky')

try:
    locations = json.loads(locationsResponse.text)
except:
    print("Greska kod dobijanja lokacija")
    exit()

# Prelazi lokaciju po lokaciju
for location in locations:
    print('--------')
    # Inicijalizuje lokaciju u dictionary-u
    allCurrent[location['id']] = {}

    # Prelazi izvor po izvor - (hereweather, openweather i darksky)
    currentHereWeather = getCurrentHereWeather({'id': location['id'], 'lat': location['Latitude'], 'lon': location['Longitude']})
    currentOpenWeather = getCurrentOpenWeather({'id': location['id'], 'lat': location['Latitude'], 'lon': location['Longitude']})
    currentDarkSky = getCurrentDarkSky({'id': location['id'], 'lat': location['Latitude'], 'lon': location['Longitude']})

    # time.sleep(1)

# Pali notifikacije
try:
    notifications = json.loads(notificationsResponse.text)
except:
    print("Greska kod dobijanja notifikacija")
    exit()

## Prolazi notifikaciju po notifikaciju
for notification in notifications:
    try:
        print('------', notification['Title'])
        # Izvlaci uslove za provjeru
        ## --- ToDo - Izvlaciti prema najpreciznijem izvoru
        ### --- To izvesti tako sto se pozovu poslednja 3 forecast-a
        ### --- za lokaciju i odabere onaj sa najboljom srednjom preciznosti
        ### --- Trenutno se koristi openweathermap jer ima najveci limit za pozive
        curConditions = allCurrent[notification['user_location']['location']]["openweathermap"]
        
        variable = notification["Variable"]
        comparison = notification["Comparison"]
        value = notification["Value"]

        ## Provjerava da li su zadovoljeni uslovi
        if comparison == "gt":
            if curConditions[variable] > value:
                ## Ako jesu, provjerava da li je vec upaljena
                ## Ako nije, salje notifikaciju
                if not notification["alreadyMet"]:
                    # Salje notifikaciju
                    sendNotification({"notification": notification})
                    # Postavlja alreadyMet
                    updateNotification({"notification": notification, "alreadyMet": True})
            else:
                ## Ako uslovi nisu zadovoljeni podesava alreadyMet na false da bi se upalila sledeci put
                if notification["alreadyMet"]:
                    # Deaktivira postojecu - mice alreadyMet
                    updateNotification({"notification": notification, "alreadyMet": False})
        elif comparison == "lt":
            if curConditions[variable] < value:
                if not notification["alreadyMet"]:
                    # Salje notifikaciju
                    sendNotification({"notification": notification})
                    # Postavlja alreadyMet
                    updateNotification({"notification": notification, "alreadyMet": True})
            else:
                if notification["alreadyMet"]:
                    # Deaktivira postojecu - mice alreadyMet
                    updateNotification({"notification": notification, "alreadyMet": False})

    except:
        print('Greska prilikom provjere notifikacije', notification['id'], notification['Title'])