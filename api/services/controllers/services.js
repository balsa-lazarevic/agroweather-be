'use strict';

// const { sanitizeEntity, parseMultipartData } = require('strapi-utils');

/* Helpers */
// const sanitize = (data, options = {}) => {
//   return sanitizeEntity(data, {
//     model: strapi.getModel('file', 'upload'),
//     ...options,
//   });
// };

/**
 * A set of functions called "actions" for `services`
 */

module.exports = {

  getLocations: async (ctx, next) => {
    try {
      /* Knex alternative */
      const knex = strapi.connections.default;
      let entities = [];
      entities = await knex('locations')
        .select()

      ctx.body = [
        ...entities
      ]

    } catch (err) {
      ctx.body = err;
    }
  }

};
