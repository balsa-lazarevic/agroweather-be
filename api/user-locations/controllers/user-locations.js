const { sanitizeEntity } = require('strapi-utils');

module.exports = {
  /**
   * Create a record.
   *
   * @return {Object}
   */

  async create(ctx) {
    let entity;
    
    let newUserLocation = ctx.request.body;

    //Validation
    //Check if user sent
    if(!ctx.request.body.user) {
        ctx.response.badRequest('invalid query');
        return
    }

    //Checks if new user location can be attached to any existing location
    const knex = strapi.connections.default;
    const locations = await knex('locations')
        .select()
    // let locations = await strapi.query('locations').find();
    let locationsMatch = locations.filter((location) => {
      let latitudeMatch = newUserLocation.Latitude >= location.Latitude - 0.1 && newUserLocation.Latitude <= location.Latitude + 0.1 ? true : false;
      let longitudeMatch = newUserLocation.Longitude >= location.Longitude - 0.1 && newUserLocation.Longitude <= location.Longitude + 0.1 ? true : false;
      if(latitudeMatch && longitudeMatch){ return location }
    })
    //Attaches to existing location if possible
    if(locationsMatch.length){
      newUserLocation.location = locationsMatch[0].id
      ctx.request.body = newUserLocation
    }
    //If not, creates new location and attaches it to it
    else {
      let newLocation = {
        Latitude: newUserLocation.Latitude,
        Longitude: newUserLocation.Longitude,
        Title: 'Kreirao korisnik ' + newUserLocation.user
      }
      entityNewLocation = await strapi.services.locations.create(newLocation);
      newUserLocation.location = entityNewLocation.id
      ctx.request.body = newUserLocation
    }

    entity = await strapi.services['user-locations'].create(ctx.request.body);
    return sanitizeEntity(entity, { model: strapi.models['user-locations'] });
  },

  async update(ctx) {
    let entity;
    if (ctx.is('multipart')) {
      const { data, files } = parseMultipartData(ctx);
      entity = await strapi.services['user-locations'].update(ctx.params, data, {
        files,
      });
    } else {
      entity = await strapi.services['user-locations'].update(
        ctx.params,
        ctx.request.body
      );
    }

    return sanitizeEntity(entity, { model: strapi.models['user-locations'] });
  },

  async find(ctx) {
    let entities;
    if (ctx.query._q) {
      entities = await strapi.services['user-locations'].search(ctx.query);
    } else {
      entities = await strapi.services['user-locations'].find(ctx.query);
    }
    
    return entities.map(entity =>
      sanitizeEntity(entity, { model: strapi.models['user-locations'] })
    );
  },

};